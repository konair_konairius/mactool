###License###

Copyright (c) 2014 Konstantin Renner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

[![GoDoc](https://godoc.org/bitbucket.org/konair_konairius/mactool?status.svg)](https://godoc.org/bitbucket.org/konair_konairius/mactool)
# Mactool #

Ever wanted to get the mac address of a remote computer over the network?

Try this:
```
#!go

package main

import (
	"fmt"

	"bitbucket.org/konair_konairius/mactool"
)

func main() {
	fmt.Println(mactool.Get("hostnameOfSomeLocalComputer"))
}

```

### What does it do? ###

* Check if the target is reachable via "ping"
* Run arp and parse the output
* Return a net.HardwareAddr Struct

### Linux Only ###

I use Linux on my Desktop and Servers so that's what i target.
It should be easy enough to add support for BSD, Windows, OS X, your Fridge or anything as log as they have system tools to access the Kernel ARP Cache and can force something to go in there (ping).

I'd be glad to help you if you need support for some OS.