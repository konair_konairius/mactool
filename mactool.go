//  Copyright (c) 2014 Konstantin Renner

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

//This is just a small libary that provides an easy way to find out the mac address of a host in the local network
package mactool

import (
	"fmt"
	"io"
	"net"
	"os/exec"
	"reflect"
	"runtime"
)

// Get is the main point of this packeage it tries to return the Mac address of a given hostname
// If someting go's wrong (probably target not reachable or on an other network) an error is returnd.
// Panics if you are not on linux
func Get(hostname string) (net.HardwareAddr, error) {
	switch runtime.GOOS {
	case "linux":
		return getLinux(hostname)
	default:
		panic("NYI")
	}
}

func getLinux(hostname string) (net.HardwareAddr, error) {

	if err := Ping(hostname); err != nil {
		return net.HardwareAddr{}, fmt.Errorf("ping failed for hostname [%s]", hostname)
	}

	runner := arpRun{Hostname: hostname}
	runner.run()
	arps, err := newArpResult(runner.tablize())
	if err != nil {
		return net.HardwareAddr{}, err
	}

	var mac string

	for _, arp := range arps {
		if mac == "" {
			mac = arp.HWaddress
			continue
		}
		if mac != arp.HWaddress {
			return net.HardwareAddr{}, fmt.Errorf("multi mac ... nyi")
		}
	}
	return net.ParseMAC(mac)
}

//Ping is used to make shure the ARP Cache is filled
//Returns an error if the Target is not Reachable
func Ping(hostname string) error {
	cmd := exec.Command("ping", "-c1", hostname)
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

type arpRun struct {
	Hostname string
	reader   io.Reader
}

func (a *arpRun) run() error {
	cmd := exec.Command("arp", a.Hostname)
	out, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	a.reader = out

	if err := cmd.Start(); err != nil {
		return err
	}
	return nil
}

func (a *arpRun) consume() (rune, bool) {
	b := make([]byte, 1)
	n, err := a.reader.Read(b)
	if err != nil && err != io.EOF {
		panic("Failed to read Stdio Stream")
	}
	return rune(b[0]), n > 0
}

func (a *arpRun) tablize() [][]string {

	var (
		lines [][]string
		line  []string
		word  []rune
		skip  bool
	)
	skip = false

	for r, next := a.consume(); next; r, next = a.consume() {
		switch r {
		case '\n':
			line = append(line, string(word))
			lines = append(lines, line)
			word = make([]rune, 0)
			line = make([]string, 0)
		case ' ':
			if !skip {
				if string(word) != "Mask" { //Handle the "Flags Mask" Field
					line = append(line, string(word))
				}
				word = make([]rune, 0)
				// fmt.Println(line)
			}
			skip = true

		default:
			skip = false
			word = append(word, r)
			// fmt.Println(word)
		}
	}
	return lines
}

type arpResult struct {
	Address   string
	HWtype    string
	HWaddress string
	Flags     string
	Iface     string
}

func newArpResult(table [][]string) ([]arpResult, error) {
	if len(table) < 2 {
		return make([]arpResult, 0), fmt.Errorf("no result from arp")
	}
	indexMap := make(map[string]int)
	t := reflect.TypeOf(arpResult{})
	result := make([]arpResult, len(table)-1)
	for i := 0; i < t.NumField(); i++ {
		indexMap[t.Field(i).Name] = -1
	}

	//Build index Map
	for i, word := range table[0] {
		if _, ok := indexMap[word]; !ok {
			fmt.Printf("[%s] not Found!\n", word)
			continue
		}
		indexMap[word] = i
	}
	for i, line := range table {
		if i == 0 {
			continue //skip headline
		}
		v := reflect.New(t)
		ve := v.Elem()
		for i := 0; i < t.NumField(); i++ {
			f := ve.Field(i)
			// fmt.Printf("%s: %v\n", t.Field(i).Name, indexMap[t.Field(i).Name])
			f.Set(reflect.ValueOf(line[indexMap[t.Field(i).Name]]))
		}
		result[i-1] = v.Elem().Interface().(arpResult)
	}

	return result, nil
}
