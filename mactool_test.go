package mactool

import (
	"testing"
)

func Test_Get(t *testing.T) {
	if _, err := Get("realrouter"); err != nil {
		t.Error(err)
	}

	if _, err := Get("NonExistentHost"); err != nil && err.Error() != "ping failed for hostname [NonExistentHost]" {
		t.Error(err)
	}

	if _, err := Get("printserver"); err != nil {
		t.Error(err)
	}
}

func Test_Ping(t *testing.T) {
	if err := Ping("realrouter"); err != nil {
		t.Error(err)
	}
	if err := Ping("wasd"); err == nil {
		t.Error("Is there a Computer called wasd in your network?")
	}
}
